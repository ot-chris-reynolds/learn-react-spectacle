
import React from "react";

const CommentForm = () => {

  return (
    //vanilla JS:

    React.DOM.form({ className: "commentForm" },
      React.DOM.input({ type: "text", placeholder: "Your name" }),
      React.DOM.input({ type: "text", placeholder: "Say something..." }),
      React.DOM.input({ type: "submit", value: "Post" })
    )
  );
};

export default CommentForm;
