import React from "react";
import styled from "styled-components";

import {
  Appear, BlockQuote, Cite, CodePane, InputExample, ComponentPlaygroundStateless, ComponentPlaygroundRecompose,
  ComponentPlaygroundStateful, Deck, Fill, Heading, Image, Layout, Link, ListItem, List, Markdown, MarkdownSlides,
  Quote, Slide, SlideSet, TableBody, TableHeader, TableHeaderItem, TableItem, TableRow, Table, Text
} from "../../src";

import preloader from "../../src/utils/preloader";

import createTheme from "../../src/themes/default";

// import Interactive from "../assets/interactive";

require("normalize.css");
require("../../src/themes/default/index.css");

const images = {
  greyDotBkg: require("../assets/gray-background-texture-002.jpg"),
  darkShade: require("../assets/blackBkg_02.jpg"),
  logo: require("../assets/otLogo.png"),
  markdown: require("../assets/markdown.png"),
  react: require("../assets/react01Bkg.png"),
  jsxExample: require("../assets/jsx-example.png"),
  jsxExampleRaw: require("../assets/jsx-example-raw.png"),
  redX: require("../assets/red-x.png"),
  reduxEx1: require("../assets/redux-illustration-simple.png"),
  reduxAction: require("../assets/redux-action.png"),
  reduxReducer: require("../assets/redux-reducer.png"),
  reactLifeCycle1: require("../assets/react-lifecycle-1.png"),
  reactLifeCycle2: require("../assets/react-lifecycle-2.png"),
  reactLifeCycle3: require("../assets/react-lifecycle-3.png"),
  reactLifeCycle4: require("../assets/react-lifecycle-4.png"),
  hocExample1: require("../assets/HOC-example-1.png")
};

preloader(images);

const theme = createTheme({
  primary: "#212121",
  secondary: "#f9f9f9",
  blue: "#40A6FF"
});
const slideColors = {
  reactBlueLt: "#28BAEB",
  reactBlueDk: "#00A6DE"
};

const SlideTextFull = styled.p`
  font-size: 0.5em;
  line-height: 1.2;
  color: white;
`;
const ReactTextStyled = styled.span`
  font-size: 1em;
  color: #F6FF4A;
  text-transform: uppercase;
`;
const SmugFace = styled.span`
  font-size: 70%;
  position: relative;
  top: -4px;
  margin: 0 .2rem;
`;

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck transition={["slide"]} theme={theme} transitionDuration={500}>
        <Slide transition={["slide"]} bgImage={images.react.replace("/", "")} bgDarken={0.1}>
          <Appear fid="1">
            <Heading size={4} margin="380px 0 0 24%" fit caps textColor="tertiary" bold>
              What is it and how do we use it?
            </Heading>
          </Appear>
        </Slide>
        <Slide transition={["fade"]} bgImage={images.greyDotBkg.replace("/", "")} bgDarken={0.8}>
          <BlockQuote>
            <Quote>
              <SlideTextFull style={{ textShadow: "6px 6px 3px #000", lineHeight: "1.4" }}>
                React isn't a framework, it's a library for building composable user interfaces. Therefore, it doesn't solve all an application's needs. It’s great at creating
                 components and providing a system for managing state for those components, but creating a more complex application requires the help of other web libraries
                  as well, all working to provide a seamless platform to develop, test, and deploy modern web applications.
              </SlideTextFull>
            </Quote>
          </BlockQuote>
        </Slide>
        <Slide transition={["fade"]} bgImage={images.greyDotBkg.replace("/", "")} bgDarken={0.8}>
          <Layout>
            <Fill>
              <List>
                <Text textColor="#FFFFFF" textSize="2.4rem" margin="0 0 1rem"><strong>Web Technologies</strong></Text>
                <Appear><ListItem margin="0.4rem 0 0"><ReactTextStyled>React</ReactTextStyled> - UI library</ListItem></Appear>
                <Appear><ListItem margin="0.4rem 0 0"><ReactTextStyled>Redux</ReactTextStyled> - application state/data</ListItem></Appear>
                <Appear><ListItem margin="0.4rem 0 0"><ReactTextStyled>Jest, Enzyme</ReactTextStyled> - JavaScript testing utility for react</ListItem></Appear>
                <Appear><ListItem margin="0.4rem 0 0"><ReactTextStyled>npm, yarn</ReactTextStyled> - node package management</ListItem></Appear>
                <Appear><ListItem margin="0.4rem 0 0"><ReactTextStyled>ES6</ReactTextStyled> - latest Javascript standards and language features</ListItem></Appear>
                <Appear><ListItem margin="0.4rem 0 0"><ReactTextStyled>Babel</ReactTextStyled> - transpile ES6 Javascript to support legacy browsers</ListItem></Appear>
                <Appear><ListItem margin="0.4rem 0 0"><ReactTextStyled>Webpack</ReactTextStyled> - source code compiling, bundling, optimizing</ListItem></Appear>
                <Appear><ListItem margin="0.4rem 0 0"><ReactTextStyled>Flow</ReactTextStyled> - enforce static typecasting for code hardening</ListItem></Appear>
                <Appear><ListItem margin="0.4rem 0 0"><ReactTextStyled>ESLint</ReactTextStyled> - static analysis utility for javascript</ListItem></Appear>
                <Appear><ListItem margin="0.4rem 0 0"><ReactTextStyled>Dev Tools</ReactTextStyled> - File watching for hot reloading, debugging</ListItem></Appear>
              </List>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["fade", "zoom"]} bgImage={images.greyDotBkg.replace("/", "")} bgDarken={0.8}>
          <Layout>
            <Fill>
              <Heading textSize="3.6rem">Why build with this stack?</Heading>
              <List>
                <Appear><Text textColor="#FFFFFF" textSize="2.4rem" lineHeight={1.2} margin="1.4rem 0">Create a library of reusable, brandable UI components</Text></Appear>
                <Appear><Text textColor="#FFFFFF" textSize="2.4rem" lineHeight={1.2} margin="1.4rem 0">Build web applications suitable for performance, maintainability, testability, reusablility, and scalability</Text></Appear>
                <Appear><Text textColor="#FFFFFF" textSize="2.4rem" lineHeight={1.2} margin="1.4rem 0">Uncouple UX from service layer architecture</Text></Appear>
                <Appear><Text textColor="#FFFFFF" textSize="2.4rem" lineHeight={1.2} margin="1.4rem 0">Provide a powerful testing platform for launching new marketing tests with less overhead and risk</Text></Appear>
                <Appear><Text textColor="#FFFFFF" textSize="2.4rem" lineHeight={1.2} margin="1.4rem 0">Avoid maintainability issues created from code duplication</Text></Appear>
                <Appear><Text textColor="#FFFFFF" textSize="2.4rem" lineHeight={1.2} margin="1.4rem 0">Provide developers and QA a rich suite of tools to improve development and testing</Text></Appear>
              </List>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["fade", "zoom"]} bgImage={images.greyDotBkg.replace("/", "")} bgDarken={0.8}>
          <Layout>
            <Fill>
              <Heading textSize="3.6rem">Current Limitations</Heading>
              <List>
                <Appear>
                  <Text textColor="#FFD1D1" lineHeight={1.2} textSize="1.2em" margin="1.6rem 0">
                    <Image src={images.redX.replace("/", "")} margin="0 5px 0 0" height={45}/>
                    SPL will be incompatible new platform services
                  </Text>
                </Appear>
                <Appear>
                  <Text textColor="#FFD1D1" lineHeight={1.2} textSize="1.2em" margin="1.6rem 0">
                    <Image src={images.redX.replace("/", "")} margin="0 5px 0 0" height={45}/>
                    Tech debt poses considerable risks when making changes or enhancements to current sign-up experience
                  </Text>
                </Appear>
                <Appear>
                  <Text textColor="#FFD1D1" lineHeight={1.2} textSize="1.2em" margin="1.6rem 0">
                    <Image src={images.redX.replace("/", "")} margin="0 5px 0 0" height={45}/>
                    Current platform poses many limitations to building data-driven experiences for future revenue-generating inititives
                  </Text>
                </Appear>
                <Appear>
                  <Text textColor="#FFD1D1" lineHeight={1.2} textSize="1.2em" margin="1.6rem 0">
                    <Image src={images.redX.replace("/", "")} margin="0 5px 0 0" height={45}/>
                    SPL's complexity and architecture hinders our team's productivity
                  </Text>
                </Appear>
                <Appear>
                  <Text textColor="#FFD1D1" lineHeight={1.2} textSize="1.2em" margin="1.6rem 0">
                    <Image src={images.redX.replace("/", "")} margin="0 5px 0 0" height={45}/>
                    And drains our souls
                  </Text>
                </Appear>
              </List>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["fade"]} bgColor={slideColors.reactBlueLt}>
          <Layout>
            <Fill>
              <Heading fit >So how does <ReactTextStyled>react</ReactTextStyled> work?</Heading>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor={slideColors.reactBlueLt}>
          <Layout>
            <Fill>
              <Heading fit >First, we have to start thinking in <ReactTextStyled>react</ReactTextStyled>.</Heading>
              <Appear><Text textColor="white" textSize="1.2em" margin="1.6rem 0">smug faces on <SmugFace>( ͡º ͜ʖ ͡º)</SmugFace></Text></Appear>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor={slideColors.reactBlueLt}>
        <Layout>
          <Fill>
            <Heading textAlign="left" textSize="3.6rem"><ReactTextStyled>React</ReactTextStyled></Heading>
            <List>
              <ListItem margin=".4rem 2.4rem">Data Flow</ListItem>
              <ListItem margin=".4rem 2.4rem">JSX</ListItem>
              <ListItem margin=".4rem 2.4rem">Types of Components</ListItem>
              <ListItem margin=".4rem 2.4rem">Component State</ListItem>
              <ListItem margin=".4rem 2.4rem">Lifecycle Methods</ListItem>
              <ListItem margin=".4rem 2.4rem">Component Examples</ListItem>
            </List>
          </Fill>
          <Fill>
            <Heading textAlign="left" textSize="3.6rem"><ReactTextStyled>Redux</ReactTextStyled></Heading>
            <List>
              <ListItem margin=".4rem 2.4rem">Application State</ListItem>
              <ListItem margin=".4rem 2.4rem">Methods</ListItem>
              <ListItem margin=".4rem 2.4rem">Actions</ListItem>
              <ListItem margin=".4rem 2.4rem">Reducers</ListItem>
              <ListItem margin=".4rem 2.4rem">Redux Middlewares</ListItem>
              <ListItem margin=".4rem 2.4rem">Redux Event Order</ListItem>
            </List>
          </Fill>
        </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor="#000">
          <Layout>
            <Fill>
              <Heading textColor="#F6FF4A" textSize="3.6rem">REACT</Heading>
              <List>
                <Appear><Text textColor="white" textSize="1.2em" margin="1.6rem 0">Component-based system for composing UI</Text></Appear>
                <Appear><Text textColor="white" textSize="1.2em" margin="1.6rem 0">Uses a "Virtual DOM" data structure and algorithm</Text></Appear>
                <Appear><Text textColor="white" textSize="1.2em" margin="1.6rem 0">Optimizes updates to the DOM for performance</Text></Appear>
                <Appear><Text textColor="white" textSize="1.2em" lineHeight={1.2} margin="1.6rem 0">Components give us modular templates that can utilize the full expressive power of Javascript in your UI</Text></Appear>
                <Appear><Text textColor="white" textSize="1.2em" margin="1.6rem 0">This allows us to build interfaces that are reusable and testable</Text></Appear>
              </List>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor="#000">
          <Layout>
            <Fill>
              <Heading textSize="3.6rem">DATA FLOW</Heading>
              <Appear>
                <Text textAlign="left" textColor="#D1F4FF" lineHeight={1.4} margin="1.6rem 0">
                  <ReactTextStyled>Unidirectional data flow</ReactTextStyled> is a technique found in functional reactive programming.
                  The major benefit of this approach is that data flows throughout your app in a
                  single direction, and you have better control over how data changes over time in your
                  application because there are less source for that data to change.
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#D1F4FF" lineHeight={1.4} margin="1.6rem 0">
                  In a React application, the data flows from the parent to the children components by the state of the props.</Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#D1F4FF" lineHeight={1.4} margin="1.6rem 0">
                  Unidirectional data flow simplifies development and debugging.
                </Text>
              </Appear>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor="#000">
          <Layout>
            <Fill>
              <Heading textSize="3.6rem">JSX</Heading>
              <Appear>
                <Text textAlign="left" textColor="#D1F4FF" margin="1.6rem 0" lineHeight={1.4}>
                  <ReactTextStyled>JSX</ReactTextStyled> is JavaScript XML syntax. It can be said that Angular and Knockout brought Javascript into HTML...
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#D1F4FF" margin="1.6rem 0" lineHeight={1.4}>
                  React brings HTML into Javascript by letting you write HTML-ish tags in your Javascript.
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#D1F4FF" margin="1.6rem 0" lineHeight={1.4}>
                  You can definitely use React without it, but JSX makes React much more elegant.
                  Just like XML, JSX tags have a tag name, attributes, and children. If an attribute value is enclosed in quotes, the value is a string.
                  Otherwise, wrap the value in braces and the value is the enclosed JavaScript expression.
                </Text>
              </Appear>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor="#000">
          <Heading textSize="3.6rem" margin="0 0 1.6rem">Example - React in Vanilla JavaScript</Heading>
          <Image src={images.jsxExampleRaw.replace("/", "")} margin="0px auto 40px" height="450px"/>
        </Slide>
        <Slide transition={["slide"]} bgColor="#000">
          <Heading textSize="3.6rem" margin="0 0 1.6rem">Example - React with JSX</Heading>
          <Image src={images.jsxExample.replace("/", "")} margin="0px auto 40px" height="450px"/>
        </Slide>
        <Slide transition={["slide"]} bgColor="#000">
          <Layout>
            <Fill>
              <Heading textSize="3.6rem">REACT COMPONENTS</Heading>
              <Appear>
                <Text textAlign="left" textColor="#D1F4FF" lineHeight={1.2} margin="1rem 0 1.6rem">
                  The two most common patterns of react components are
                   called <ReactTextStyled>Stateless</ReactTextStyled> and <ReactTextStyled>Stateful</ReactTextStyled>
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#D1F4FF" lineHeight={1.5} margin="0 0 1.6rem">
                  <ReactTextStyled>Stateful</ReactTextStyled> components contain logic to maintain their own state as well as utilize react's component lifecycle methods,
                  while <ReactTextStyled>Stateless</ReactTextStyled> components will just return a piece of UI based on its props.
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#D1F4FF" lineHeight={1.5} margin="0 0 1.6rem">
                  Component state is typically not visible outside of the component,
                  and is simply internal state that is required for a component to do its job.
                </Text>
              </Appear>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor={slideColors.reactBlueLt}>
          <Layout>
            <Fill>
              <Heading margin="0 0 1.6rem" textSize="3.6rem">STATELESS FUNCTIONAL COMPONENTS</Heading>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.2} margin="0 0 1.6rem">
                  In an ideal application, most components would be pure, stateless functions that simply return markup based on its props.
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.2} margin="0 0 1.6rem">
                  These components focus on the UI rather than behavior.
                </Text>
              </Appear>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor={slideColors.reactBlueLt}>
          <Layout>
            <Fill>
              <Heading margin="0 0 1.6rem" textSize="3.6rem">STATEFUL COMPONENTS</Heading>
              <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.2} margin="0 0 1.6rem">Stateful components maintain state of itself and it's children. These components are aware of changes to data and can choose what to do with those changes.</Text>
              <Appear><Text textAlign="left" margin=".5rem 0 1rem"><ReactTextStyled>Example use cases:</ReactTextStyled></Text></Appear>
              <Appear><Text textAlign="left" textColor="#FFFFFF" margin="0 0 1rem">Your component needs behavior that requires <strong>Internal State</strong></Text></Appear>
              <Appear><Text textAlign="left" textColor="#FFFFFF" margin="0 0 1rem">You need to utilize <strong>Lifecycle Methods</strong></Text></Appear>
              <Appear><Text textAlign="left" textColor="#FFFFFF" margin="0 0 1rem">You need to fetch data</Text></Appear>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor="#000">
          <Layout>
            <Fill>
              <Heading textSize="3.6rem">REACT LIFECYCLE METHODS</Heading>
              <Appear>
                <Text textSize="1.2em" textAlign="left" textColor="#FFFFFF" margin="1rem 0 0.8rem" lineHeight={1.2}>
                  Stateful components have lifecycle methods you can use to run code at particular times during runtime. Usually these are used for managing internal state or fetching data.
                </Text>
              </Appear>
              <Appear>
                <Text textSize="1.2em" textAlign="left" textColor="#D1F4FF" margin="0 0 0.8rem" lineHeight={1.4}>
                  <strong>constructor()</strong> - called before the component is mounted, typically used to initialize a component's internal state
                </Text>
              </Appear>
              <Appear>
                <Text textSize="1.2em" textAlign="left" textColor="#D1F4FF" margin="0 0 0.8rem" lineHeight={1.4}>
                  <strong>componentWillMount()</strong> - invoked before executing render()
                </Text>
              </Appear>
              <Appear>
                <Text textSize="1.2em" textAlign="left" textColor="#D1F4FF" margin="0 0 0.8rem" lineHeight={1.4}>
                  <strong>componentDidMount()</strong> - great for making api calls to fetch data
                </Text>
              </Appear>
              <Appear>
                <Text textSize="1.2em" textAlign="left" textColor="#D1F4FF" margin="0 0 0.8rem" lineHeight={1.4}>
                  <strong>componentWillUpdate()</strong> - invoked when the component is updated
                </Text>
              </Appear>
              <Appear>
                <Text textSize="1.2em" textAlign="left" textColor="#D1F4FF" margin="0 0 0.8rem" lineHeight={1.4}>
                  <strong>shouldComponentUpdate()</strong> - determine if a change in state or props requires re-render of the component
                </Text>
              </Appear>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["fade"]} bgImage="#222">
              <Image src={images.reactLifeCycle1.replace("/", "")} height="206px"/>
              <Appear>
                <Image margin="0 0 0 -60px" src={images.reactLifeCycle2.replace("/", "")} height="105px"/>
              </Appear>
              <Appear>
                <Image margin="0 0 0 0" src={images.reactLifeCycle3.replace("/", "")} height="115px"/>
              </Appear>
              <Appear>
                <Image margin="0 0 0 -560px" src={images.reactLifeCycle4.replace("/", "")} height="108px"/>
              </Appear>
        </Slide>
        <Slide transition={["slide"]} bgColor="#000">
          <Layout>
            <Fill>
              <Heading textSize="3.6rem">HIGHER-ORDER COMPONENTS</Heading>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.4} margin="1rem 0 1rem">
                  Due to React's compositional nature, you can wrap a component in a Higher Order Component (HOC) to enhanced the wrapped component without modifying the underlying component. Essentially it's a functions that returns another function.
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.4}>
                  Common patterns for HOCs are:<br />
                  Code reuse, props manipulation, logic and bootstrap abstraction, render highjacking, or state abstraction and manipulation.
                  HOCs are common in third-party React libraries, such as Redux's connect
                </Text>
              </Appear>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor="#000">
          <Layout>
            <Fill>
              <Heading margin="0 0 1rem" textSize="3.6rem">SIMPLE HOC EXAMPLE</Heading>
              <Image margin="0 0 0 -60px" src={images.hocExample1.replace("/", "")} height="296px"/>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["zoom", "fade"]} bgColor="primary">
          <Heading textSize="3.6rem" margin="0 0 1.2rem">COMPONENT EXAMPLES</Heading>
          <InputExample type="text"/>
        </Slide>
        <Slide>
          <Heading textSize="3.6rem" margin="0 0 3.6rem">STATELESS FUNCTIONAL COMPONENT</Heading>
          <ComponentPlaygroundStateless
            theme="dark"
          />
        </Slide>
        <Slide>
          <Heading textSize="3.6rem" margin="0 0 3.6rem">HOC WRAPPED STATELESS FUNCTIONAL</Heading>
          <ComponentPlaygroundRecompose
            theme="dark"
          />
        </Slide>
        <Slide>
          <Heading textSize="3.6rem" margin="0 0 3.6rem">STATEFUL COMPONENT</Heading>
          <ComponentPlaygroundStateful
            theme="dark"
          />
        </Slide>
        <Slide transition={["slide"]} bgColor={slideColors.reactBlueLt}>
          <Heading textSize="3.6rem">REDUX OVERVIEW</Heading>
          <Layout>
            <Fill>
              <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.4} margin="1rem 0 1rem">
                While React handles the view layer of our application, a popular choice for managing and maintaining the application state
                is a JavaScript framework called <ReactTextStyled>Redux</ReactTextStyled>. Whether your app is really simple or complex with a lot of UI and change of state,
                all data to be persisted outside of your components is contained in a single object we call the redux state tree.
              </Text>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor={slideColors.reactBlueLt}>
          <Layout>
            <Fill>
              <Heading textSize="3.6rem">STATE AND REDUX</Heading>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.4} margin="0 0 1rem">
                  Data naturally flows down through the application tree; from the top level components down to the smaller components.
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.4} margin="1rem 0 1rem">
                  There are two types of state to consider when designing a react app: <strong>component state</strong> and <strong>application state</strong>.
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.4} margin="1rem 0 1rem">
                  <strong>Component state</strong> is used for properties on a component that will change, versus static properties that are passed in. <br />
                   <strong>Application state</strong> contains the the Immutable tree of data to be persisted for the entire application.
                </Text>
              </Appear>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor="#000">
          <Heading textSize="3.6rem">REDUX OVERVIEW CONTINUED</Heading>
          <Layout>
            <Fill>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.2} margin="1rem 0 1rem">
                  <strong>1. Single Source of Truth </strong>
                   - The state of your whole application is stored in a single object tree; the "Store".
                  This makes it much easier to keep track of the state of your application at any time and roll back to any previous state.
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.2} margin="0 0 1rem">
                  <strong>2. State is Read-Only ("Immutable") </strong>
                   - Instead of directly updating data in the store, we describe the update as a function which gets applied to the existing store and returns a new version.
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.2} margin="0 0 1rem">
                  <strong>3. Changes are made using Pure Functions </strong>
                   - To change the state tree we use "actions" that call "reducers", these are simple functions which perform a single action.
                </Text>
              </Appear>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor="#000">
          <Heading textSize="3.6rem">ACTIONS</Heading>
          <Layout>
            <Fill>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.4} margin="1rem 0 1rem">
                  <ReactTextStyled>Actions</ReactTextStyled> are events in our application. Actions usually describe what happened, and contain the data that changed.
                  To update the state tree, we dispatch an action containing a plain JavaScript object describing the change.
                </Text>
              </Appear>
              <Appear>
                <Image src={images.reduxAction.replace("/", "")} height="250px"/>
              </Appear>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor="#000">
          <Heading textSize="3.6rem">REDUCERS</Heading>
          <Layout>
            <Fill>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.4} margin="1rem 0 1rem">
                  Actions describe the fact that something happened, but don't specify how the application's state changes in response. This is the job of <ReactTextStyled>reducers</ReactTextStyled>.
                  The reducer is a pure function that takes the state of the whole application with the action being dispatched and returns the next state of the application.
                </Text>
              </Appear>
              <Appear>
                <Image src={images.reduxReducer.replace("/", "")} height="330px"/>
              </Appear>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgColor="#000">
          <Heading textSize="3.6rem">REDUX METHODS</Heading>
          <Layout>
            <Fill>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.2} margin="1rem 0 1rem">
                  Redux's store has three important methods:
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.2} margin="0 0 1rem">
                  <strong>getState()</strong> - retrieves the current state of the store.
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.2} margin="0 0 1rem">
                  <strong>dispatch()</strong> - lets you dispatch actions to update the store. <br />
                  Takes an action as a parameter => dispatch( someAction() );
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.2} margin="0 0 1rem">
                  <strong>subscribe()</strong> - lets you register a callback that Redux will envoke when an action has been dispatched.
                </Text>
              </Appear>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["fade"]} bgColor="#000">
          <Heading textSize="3.6rem" margin="0 0 1.6rem">REDUX DESIGN</Heading>
          <Image margin="0 0 0 -3rem" src={images.reduxEx1.replace("/", "")} height="450px"/>
        </Slide>
        <Slide transition={["slide"]} bgColor="#000">
          <Heading textSize="3.6rem">REDUX MIDDLEWARE</Heading>
          <Layout>
            <Fill>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.4} margin="1rem 0 1rem">
                  To put it simply, Redux middleware is a function which is invoked after an action is dispatched, but before its reducer is executed.
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.4} margin="1rem 0 1rem">
                  If you wanted to add logging or fetch data based on the action being dispatched,
                   Redux middleware provides a point between dispatching actions to execute these 'side effects'.
                </Text>
              </Appear>
              <Appear>
                <Text textAlign="left" textColor="#FFFFFF" lineHeight={1.4} margin="1rem 0 1rem">
                   Right now the most popular middlewares used to handle asynchronous actions in Redux are Thunks and Sagas.
                </Text>
              </Appear>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgImage={images.greyDotBkg.replace("/", "")} bgDarken={0.8}>
          <Layout>
            <Fill>
              <Heading textSize="3.6rem">REDUX EVENT ORDER</Heading>
              <List>
                <Appear><Text textColor="#FFFFFF" lineHeight={1.2} textSize="2.8rem" margin="1.6rem 0">Components can be passed callback functions as props, which can dispatch actions</Text></Appear>
                <Appear><Text textColor="#FFFFFF" lineHeight={1.2} textSize="2.8rem" margin="1.6rem 0">Those callbacks dispatch actions containing data about what happened</Text></Appear>
                <Appear><Text textColor="#FFFFFF" lineHeight={1.2} textSize="2.8rem" margin="1.6rem 0">Reducers process the actions, computing the new state</Text></Appear>
                <Appear><Text textColor="#FFFFFF" lineHeight={1.2} textSize="2.8rem" margin="1.6rem 0">The new state of the whole application goes into a single store.</Text></Appear>
                <Appear><Text textColor="#FFFFFF" lineHeight={1.2} textSize="2.8rem" margin="1.6rem 0">Components receive the new state as props and re-render themselves where needed.</Text></Appear>
              </List>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgImage={images.greyDotBkg.replace("/", "")} bgDarken={0.8}>
          <Layout>
            <Fill>
              <Heading textSize="3.6rem">SOME TIPS FOR REACT</Heading>
              <List>
                <Appear><Text textColor="#FFFFFF" lineHeight={1.2} textSize="2.8rem" margin="1.6rem 0">Try to keep most of your components small and stateless</Text></Appear>
                <Appear><Text textColor="#FFFFFF" lineHeight={1.2} textSize="2.8rem" margin="1.6rem 0">TDD - Write valueable unit tests for all your components, action handlers, and reducers</Text></Appear>
                <Appear><Text textColor="#FFFFFF" lineHeight={1.2} textSize="2.8rem" margin="1.6rem 0">Keep business logic in stateful components, not in presentational components</Text></Appear>
                <Appear><Text textColor="#FFFFFF" lineHeight={1.2} textSize="2.8rem" margin="1.6rem 0">Remember that adding state makes your component harder to test and reason about</Text></Appear>
              </List>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide"]} bgImage={images.greyDotBkg.replace("/", "")} bgDarken={0.8}>
          <Layout>
            <Fill>
              <Heading textSize="3.6rem">Links to get started</Heading>
              <List>
                <Link target="_blank" href="https://facebook.github.io/react/tutorial/tutorial.html">
                  <Text margin="0 0 1rem" bold caps textColor="tertiary">Tutorial: Intro To React - React</Text>
                </Link>
                <Link target="_blank" href="https://egghead.io/courses/getting-started-with-redux">
                  <Text margin="0 0 1rem" bold caps textColor="tertiary">Building Applications with React and Redux</Text>
                </Link>
                <Link target="_blank" href="https://app.pluralsight.com/library/courses/react-redux-react-router-es6">
                  <Text margin="0 0 1rem" bold caps textColor="tertiary">Getting Started with Redux</Text>
                </Link>
                <Link target="_blank" href="https://gitlab.com/ot-chris-reynolds/learn-react-spectacle">
                  <Text margin="0 0 1rem" bold caps textColor="tertiary">View these Slides - Written in React!</Text>
                </Link>
              </List>
            </Fill>
          </Layout>
        </Slide>
        <Slide transition={["slide", "spin"]} bgColor="primary">
          <Heading caps fit size={1} textColor="tertiary">
            DISCUSSION
          </Heading>
          <Heading caps fit size={1} textColor="secondary">
            QUESTIONS / COMMENTS
          </Heading>
        </Slide>
      </Deck>
    );
  }
}
