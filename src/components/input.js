import React, { PropTypes } from "react";
import styled, { css } from "styled-components";

export const fontSize = ({ height }) => `${height / 35.5555555556}rem`;

const styles = css`
  width: 100%;
  height: auto;
  padding: 0 1rem;
  font-size: 3.4vw;
  font-family: 'Myriad Pro', sans-serif;
  line-height: 1.42857143;
  color: #333333;
  vertical-align: middle;
  background-color: #ffffff;
  border: 1px solid #9f9f9f;
  -webkit-transition: border-color ease-in-out .25s;
  -moz-transition: border-color ease-in-out .25s;
  -o-transition: border-color ease-in-out .25s;
  transition: border-color ease-in-out .25s;

  &[type=checkbox], &[type=radio] {
    display: inline-block;
    border: 0;
    border-radius: 0;
    width: auto;
    height: auto;
    margin: 0 0.2rem 0 0;
  }
`;

const StyledTextarea = styled.textarea`${styles}`;
const StyledSelect = styled.select`${styles}`;
const StyledInput = styled.input`${styles}`;

const Input = ({ ...props }) => {
  if (props.type === "textarea") {
    return <StyledTextarea {...props} />;
  } else if (props.type === "select") {
    return <StyledSelect {...props} />;
  }
  return <StyledInput {...props} />;
};

Input.propTypes = {
  height: PropTypes.number,
  invalid: PropTypes.bool,
  reverse: PropTypes.bool,
  type: PropTypes.string
};

Input.defaultProps = {
  type: "text",
  height: 40
};

export default Input;
