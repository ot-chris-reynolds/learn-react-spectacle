import React, { Component, PropTypes } from "react";
import styled, { css } from "styled-components";
import Input from "./input";
import Radium from "radium";

const FieldWrapper = styled.div`
  background-color: #fff;
  padding: 2rem;
  border: 4px solid #222;
  border-radius: .5em;
`;
const Label = styled.label`
  display: -webkit-flex;
  display: flex;
  -webkit-flex-direction: row;
  flex-direction: row;
  -webkit-align-items: center;
  align-items: center;
  -webkit-justify-content: center;
  justify-content: center;
  span {
    width: 36%;
    font-size: 3.2rem;
    font-weight: 700;
    color: #333333;
  }
  input {
    width: 64%;
    padding: .4rem;
    font-size: 2.4rem;
  }
`;

@Radium
export default class InputExample extends Component {
  render() {

    return (
      <FieldWrapper {...this.props}>
        <Label>
          <span>Input Field:</span>
          <Input type="text" />
        </Label>
      </FieldWrapper>
    );
  }
}

InputExample.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  style: PropTypes.object
};

InputExample.contextTypes = {
  styles: PropTypes.object,
  store: PropTypes.object,
  typeface: PropTypes.object
};
