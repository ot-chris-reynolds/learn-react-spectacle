export const defaultCode = `
/**
 * Sample React Component
 * Output domContainerNode is 'mountNode'
 */

// import styles from './styles.scss';

class MyComponent extends React.Component {

  constructor() {
    /* 'this' context is uninitialized until
       super() is invoked in constructor method  */
    super();
    /*   define initial component state  */
    this.state = {
      inputValue: ''
    };
  }
  /* attach method to component for
     handling our submit action  */
  handleChange = (e) => {
    this.setState({ inputValue: e.target.value });
  }

  render() {
    return (
      <div style={styles.card}>
        <h1 style={styles.heading}>GREETINGS,</h1>
        <h4 style={styles.name}>{this.state.inputValue || "NAME WILL GO HERE"}</h4>
        <label style={styles.inputLabel}>
          <span style={styles.labelText}>Input Field</span>
          <input
            type="text"
            style={styles.inputField}
            onChange={this.handleChange} />
        </label>
      </div>
    )
  }
}

const styles = {
  card: {
    border: "1px solid #343434",
    padding: "1rem",
    background: "#eee"
  },
  heading:  {
    fontSize: "1.5rem",
    color: "#2B2B2B",
    textAlign: "center",
    margin: "0 0 .8rem",
    textTransform: "uppercase"
  },
  name: {
    fontSize: "1.8rem",
    textAlign: "center",
    margin: "0 0 1rem",
    fontWeight: "600",
    minHeight: "1.4rem",
    backgroundColor: "#E3E3E3",
    border: "2px solid #D9D9D9",
    color: "#000000",
    padding: ".4rem"
  },
  fieldWrapper: {
    backgroundColor: "#fff",
    padding: "1.4rem",
    border: "2px solid #222",
    borderRadius: ".8rem",
    boxShadow: "1px 1px 2px #3e3e3e"
  },
  inputLabel: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  labelText: {
    width: "36%",
    fontSize: "1.6rem",
    fontWeight: "600",
    color: "#333333"
  },
  inputField: {
    width: "64%",
    padding: ".4rem",
    fontSize: "1.4rem"
  }
};

render(
  <MyComponent />,
  mountNode)
`;
