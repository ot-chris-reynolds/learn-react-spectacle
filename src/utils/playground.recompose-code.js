export const defaultCode = `
/**
 * Sample React Component
 * Output domContainerNode is 'mountNode'
 */

const MyComponent = ({ name, setName }) => (
  <div style={styles.fieldWrapper}>
    <h1 style={styles.heading}>Hello, {name}</h1>
    <label style={styles.inputLabel}>
      <span style={styles.labelText}>Input Field:</span>
      <Input style={styles.inputField} type="text" onChange={ e => setName(e.target.value) } />
    </label>
  </div>
)

const styles = {
  heading:  {
    fontSize: "2.2rem",
    textAlign: "center",
    margin: "0 0 1.4rem",
    background: "#262626",
    color: "#fff",
    padding: "1.2rem",
    borderRadius: ".8rem",
    overflow: "hidden"
  },
  fieldWrapper: {
    backgroundColor: "#fff",
    padding: "1.4rem",
    border: "2px solid #222",
    borderRadius: ".8rem",
    boxShadow: "1px 1px 2px #3e3e3e"
  },
  inputLabel: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  labelText: {
    width: "36%",
    fontSize: "1.6rem",
    fontWeight: "600",
    color: "#333333"
  },
  inputField: {
    width: "64%",
    padding: ".4rem",
    fontSize: "1.4rem"
  }
}

const EnhancedComponent = withState("name", "setName", props => props.name)(MyComponent);

render(
  <EnhancedComponent name="Topher" />,
  mountNode)
`;
