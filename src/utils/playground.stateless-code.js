export const defaultCode = `
/**
 * Sample React Component
 * Output domContainerNode is 'mountNode'
 */

const TextField = ( { label } ) => (
  <label style={styles.inputLabel}>
    <span style={styles.labelText}>{label}</span>
    <input style={styles.inputField} type="text" />
  </label>
)

const styles = {
  inputLabel: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  labelText: {
    width: "36%",
    fontSize: "1.6rem",
    fontWeight: "600",
    color: "#333333"
  },
  inputField: {
    width: "64%",
    padding: ".4rem",
    fontSize: "1.4rem"
  }
}

render(
  <TextField label="Input Field"/>,
  mountNode)
`;
